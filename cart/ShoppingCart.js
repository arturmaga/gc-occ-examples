define('knockout', 'pubsub', 'jquery', 'ccLogger', 'viewModels/address', 'ccConstants', function (ko, pubsub, $, ccLogger, Address, ccConstants) {
    'use strict';
    return {
        cartProducts: ko.observableArray([]),
        onLoad: function () {
            // ccLogger.error('Chamar o CCLogger para erro');
            // ccLogger.info('Chamar o CCLogger para info');
            $.Topic(pubsub.topicNames.SHIPPING_METHODS_LOADED).subscribe(function (data) {
                console.info('Carregou o frete', data, widget.shippingmethods());
            });
        },

        /**
         * Verifica se o usuário está logado
         */
        isLogged: function () {
            var widget = this;
            if (widget.user().loggedIn()) { // verifica se o usuário está logado
                console.info('logged');
            }
        },

        /**
         * Faz o login
         */
        doLogin: function () {
            var widget = this;
            var login = 'abc';
            var password = '12356';
            widget.user().login(login);
            widget.user().password(password);
            widget.user().updateLocalData(true, false);
            widget.user().loggedinAtCheckout(true);
            $.Topic(pubsub.topicNames.USER_LOGIN_SUBMIT).publishWith(widget.user(), [{message: 'success'}]);
        },

        updateCart: function (product) {
            var newProduct = $.extend(true, product);
            $.Topic(pubsub.topicNames.CART_ADD).publishWith(newProduct, [{message: 'success'}]);
        },
        addQuantity: function (cartItem) {
            // Implementação de alguma lógica para validar o estoque
            var quantity = parseInt(cartItem.quantity, 10);
            var maxQuantity = 99;
            quantity++;
            if (quantity <= maxQuantity) {
                cartItem.quantity = quantity;
                this.updateCart(cartItem);
            }
        },
        subQuantity: function (cartItem) {
            // Implementação de alguma lógica para validar o estoque
            var quantity = parseInt(cartItem.quantity, 10);
            var maxQuantity = 99;
            quantity--;
            if (quantity > 0) {
                cartItem.quantity = quantity;
                this.updateCart(cartItem);
            }
        },
        remove: function (cartItem) {
            $.Topic(pubsub.topicNames.CART_REMOVE).publishWith(cartItem.productData(), [{
                'message': 'success',
                'commerceItemId': cartItem.commerceItemId
            }]);
        },

        beforeAppear: function () {
            var widget = this;
            // aqui você pega todos os itens do array
            widget.cartProducts(widget.cart().allItems());
            var addToCartSuccedded = function () {
                console.info('// Atualizou o carrinho');
                $.Topic(pubsub.topicNames.CART_ADD_SUCCESS).unsubscribe(addToCartSuccedded);
            };
            $.Topic(pubsub.topicNames.CART_ADD_SUCCESS).subscribe(addToCartSuccedded);

            $.Topic(pubsub.topicNames.USER_LOGIN_SUCCESSFUL).subscribe(function (obj) {
               console.info('Usuario logado');
            });

            $.Topic(pubsub.topicNames.USER_LOGIN_FAILURE).subscribe(function (obj) {
                console.info('Falha no login');
            });
        },

        applyCoupon: function (coupon) {
            // Adiciona um cupom ao carrinho, caso der erro ele vai aparecer na barra de notificacoes do ecomm
            widget.cart().addCoupon(coupon);
        },


        calculateShipping: function (zipCode) {
            var shippingAddress = new Address('user-shipping-address', widget.ErrorMsg, widget, widget.shippingCountries(), widget.defaultShippingCountry());

            var resultCep = {}; // aqui vem o resultado de alguma ferramenta externa que pega os dados do CEP do usuario e traz estado, cdidade etc...

            // seta o objeto para ser enviado
            shippingAddress.address1(resultCep.address1);
            shippingAddress.address2(resultCep.address2);
            shippingAddress.address3(resultCep.address3);
            shippingAddress.city(resultCep.city);
            shippingAddress.state(resultCep.state);
            shippingAddress.selectedCountry('BR');
            shippingAddress.postalCode(zipCode);

            // prepara os dados para serem enviados ao calculo do OCC
            var shippingAddressWithProductIDs = {};
            shippingAddressWithProductIDs[ccConstants.SHIPPING_ADDRESS_FOR_METHODS] = shippingAddress;
            shippingAddressWithProductIDs[ccConstants.PRODUCT_IDS_FOR_SHIPPING] = widget.cart().getProductIdsForItemsInCart();

            // dispara o topico de calculo
            $.Topic(pubsub.topicNames.POPULATE_SHIPPING_METHODS).publishWith(shippingAddressWithProductIDs, [{message: 'success'}]);

        }
    }
});
