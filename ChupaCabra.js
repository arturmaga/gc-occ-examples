XMLHttpRequest.prototype.realSend = XMLHttpRequest.prototype.send;
XMLHttpRequest.prototype.send = function(value) {
    this.addEventListener("progress", function(){
        console.log("Progress Loading");
    }, false);

    this.addEventListener("load", function(){
        console.log(this.responseText);
    }, false);

    this.realSend(value);
};
